<pre>
<?php
require './connection.php';
$db= new dbConnector('localhost','root','3068145','safar');

class asa_read_data_class {
    public $source_id=3;
    public $db;  
    function __construct($db) {
        $this->db=$db;
        //$this->read_hotels();
     // $this->room_type();
       $this->room_avail();
    }
    static function file_get_contents_utf8($url) { 
        $opts = array( 
            'http' => array( 
                'method'=>"GET", 
                'header'=>"Content-Type: text/html; charset=utf-8" 
            ) 
        ); 

        $context = stream_context_create($opts); 
        return  @file_get_contents($url,false,$context); 
        
    } 
    
    public function read_hotels(){
        $asa_hotel=array();
        $url="http://94.183.153.237/safar/hotels_list.php";
        $result = $this->file_get_contents_utf8($url); 
        if( $result == false){ return false; }
        else {
            $result= json_decode($result,true);
            //print_r($result);
            foreach ($result as $a=>$hotel){
                $asa_hotel[$hotel['id']]['name']=$hotel['persian_name'];
                $asa_hotel[$hotel['id']]['en_name']=$hotel['foreign_name'];
            }
            $this->db->addHotelSource($this->source_id,$asa_hotel);            
        }
    }
    
    public function room_type(){
        $asa_room_type=array();      
        $url="http://94.183.153.237/safar/roomkinds_list.php";
        $result = $this->file_get_contents_utf8($url); 
        $result= json_decode($result,true);
//        return $result;
        if( $result == false){ return false; }
        else {
            //print_r($result);
            foreach ($result as $room){
                $asa_room_type[$room['id']]['name']= $room['persian_name'];
                $asa_room_type[$room['id']]['en_name']=$room['foreign_name'];
            }
            $this->db->addRoomType($this->source_id,$asa_room_type);
            }
        }
    
    public function room_avail(){
            $url="http://94.183.153.237/safar/capacity_list.php";
            $result = $this->file_get_contents_utf8($url); 
            if( $result == false){ return false; }
            $result= json_decode($result,true);
            $hotel_id=  $this->db->listHotelMin($this->source_id);
            $room_id=  $this->db->listRoomMin($this->source_id);
            $khadamat=array();
            $i=0;
            //print_r($result);
            foreach ($result as $capacity){
               //  echo $room_id[$capacity['room_kind_id']].' '.$hotel_id[$capacity['id_hotel']].'<br>';
                    if(isset($room_id[$capacity['room_kind_id']]) && isset($hotel_id[$capacity['id_hotel']])){
                        //mysqli_query($db,"INSERT INTO `hotel_avail` (`room_type_id`,`hotels_id`,`tarikh`,`capa`,`price`,`currency_id`,`en`,`source_id`) VALUES (".$room_id[$capacity['room_kind_id']].",".$hotel_id[$capacity['id_hotel']].",'$capacity[record_timestamp]','$capacity[capacity]','$capacity[iranian_board_rate]',1,3,".$this->source_id.") ");      
                        $this->db->addHotelAvail($room_id[$capacity['room_kind_id']],$hotel_id[$capacity['id_hotel']],$capacity['record_timestamp'],$capacity['capacity'],$capacity['iranian_board_rate'],1,3,$this->source_id);
                        //$hotel_avail_id= $this->db->insert_id();
                        $hotel_avail_id=0;
                        if($capacity['extra_bed']!=0) {
                            $i++;
                            $khadamat[$i]['hotel_avail_id']=$hotel_avail_id;
                            $khadamat[$i]['khadamat_id']=1;
                            $khadamat[$i]['val']=$capacity['extra_bed'];
                        }
                        if($capacity['percent']!='0.00'){
                            $i++;
                            $khadamat[$i]['hotel_avail_id']=$hotel_avail_id;
                            $khadamat[$i]['khadamat_id']=2;
                            $khadamat[$i]['val']=$capacity['percent'];
                        }
                        if($capacity['lunch_room']!=0){
                            $i++;
                            $khadamat[$i]['hotel_avail_id']=$hotel_avail_id;
                            $khadamat[$i]['khadamat_id']=3;
                            $khadamat[$i]['val']=$capacity['lunch_room'];
                        }
                        if($capacity['dinner_room']!=0) {
                            $i++;
                            $khadamat[$i]['hotel_avail_id']=$hotel_avail_id;
                            $khadamat[$i]['khadamat_id']=4;
                            $khadamat[$i]['val']=$capacity['dinner_room'];
                        }
                        if($capacity['dinner_person']!=0){
                            $i++;
                            $khadamat[$i]['hotel_avail_id']=$hotel_avail_id;
                            $khadamat[$i]['khadamat_id']=5;
                            $khadamat[$i]['val']=$capacity['dinner_person'];
                        }
                        if($capacity['lunch_person']!=0) {
                            $i++;
                            $khadamat[$i]['hotel_avail_id']=$hotel_avail_id;
                            $khadamat[$i]['khadamat_id']=6;
                            $khadamat[$i]['val']=$capacity['lunch_person'];
                        }
                        if($capacity['lunch_dinner_person']!=0){
                            $i++;
                            $khadamat[$i]['hotel_avail_id']=$hotel_avail_id;
                            $khadamat[$i]['khadamat_id']=7;
                            $khadamat[$i]['val']=$capacity['lunch_dinner_person'];
                        }
                        if($capacity['all']!=0){
                            $i++;
                            $khadamat[$i]['hotel_avail_id']=$hotel_avail_id;
                            $khadamat[$i]['khadamat_id']=9;
                            $khadamat[$i]['val']=$capacity['all'];
                        }
                        if($capacity['uall']!=0){
                            $i++;
                            $khadamat[$i]['hotel_avail_id']=$hotel_avail_id;
                            $khadamat[$i]['khadamat_id']=10;
                            $khadamat[$i]['val']=$capacity['uall'];
                        }
                        if($capacity['persian_hotel_label']!=''){
                            $i++;
                            $khadamat[$i]['hotel_avail_id']=$hotel_avail_id;
                            $khadamat[$i]['khadamat_id']=8;
                            $khadamat[$i]['val']=$capacity['persian_hotel_label'];
                        }
                    }
            }
            $this->db->addAvailRoomProperty($khadamat);
         //__________ UPDATE en1
            $this->db->query("UPDATE `hotel_avail` SET `en`=2 WHERE `en`=1 AND `source_id`=".$this->source_id." ");
            $this->db->query("UPDATE `hotel_avail` SET `en`=1 WHERE `en`=3 AND `source_id`=".$this->source_id." ");
            $this->db->query("DELETE FROM `hotel_avail`  WHERE `en`=2 AND `source_id`=".$this->source_id." ");
            $this->db->query("DELETE FROM `khadamat_room`  WHERE `en`=1 AND `source_id`=".$this->source_id." ");
            $this->db->query("UPDATE `khadamat_room` SET `en`=1 WHERE `en`=3 AND `source_id`=".$this->source_id." ");
        }
           
        
        
    function room_avail2(){
            $url="http://94.183.153.237/safar/capacity_list.php";
            $result = $this->file_get_contents_utf8($url); 
            if( $result == false){ return false; }
            else {
                $hid=  mysqli_query($db,"SELECT hotels_id,id_source_hotel FROM `hotels_source` where source_id=".$this->source_id." ");
                while($row=  mysqli_fetch_assoc($hid)){
                    $hotel_id[$row['id_source_hotel']]=$row['hotels_id'];
                }

                $rid=  mysqli_query($db,"SELECT id,id_room_source FROM `room_type` where source_id=".$this->source_id." ");
                while($row=  mysqli_fetch_assoc($rid)){
                    $room_id[$row['id_room_source']]=$row['id'];
                }

                //$insert=$db->prepare("INSERT INTO `hotel_avail` (`room_type_id`,`hotels_id`,`tarikh`,`capa`,`price`,`currency_id`,`en`,`source_id`) VALUES (:room_type_id,:hotels_id,:tarikh,:capa,:price,:currency_id,:en,:source_id) ");      
                //$insert_khadamat=$db->prepare("INSERT INTO `khadamat_room` (`hotel_avail_id`,`khadamat_id`,`val`,`en`) VALUES (:hotel_avail_id,:khadamat_id,:val,3)");              
                
                $result= json_decode($result,true);
                foreach ($result as $capacity){
                    if(isset($room_id[$capacity['room_kind_id']]) && isset($hotel_id[$capacity['id_hotel']])){
                        mysqli_query($db,"INSERT INTO `hotel_avail` (`room_type_id`,`hotels_id`,`tarikh`,`capa`,`price`,`currency_id`,`en`,`source_id`) VALUES (".$room_id[$capacity['room_kind_id']].",".$hotel_id[$capacity['id_hotel']].",'$capacity[record_timestamp]','$capacity[capacity]','$capacity[iranian_board_rate]',1,3,".$this->source_id.") ");      
                        $hotel_avail_id=  mysqli_insert_id($db);
                        if($capacity['extra_bed']!=0)           mysqli_query($db,"INSERT INTO `khadamat_room` (`hotel_avail_id`,`khadamat_id`,`val`,`en`) VALUES ($hotel_avail_id,1,'$capacity[extra_bed]',3)");
                        if($capacity['percent']!='0.00')        mysqli_query($db,"INSERT INTO `khadamat_room` (`hotel_avail_id`,`khadamat_id`,`val`,`en`) VALUES ($hotel_avail_id,2,'$capacity[extra_bed]',3)");
                        if($capacity['lunch_room']!=0)          mysqli_query($db,"INSERT INTO `khadamat_room` (`hotel_avail_id`,`khadamat_id`,`val`,`en`) VALUES ($hotel_avail_id,3,'$capacity[extra_bed]',3)");
                        if($capacity['dinner_room']!=0)         mysqli_query($db,"INSERT INTO `khadamat_room` (`hotel_avail_id`,`khadamat_id`,`val`,`en`) VALUES ($hotel_avail_id,4,'$capacity[extra_bed]',3)");
                        if($capacity['dinner_person']!=0)       mysqli_query($db,"INSERT INTO `khadamat_room` (`hotel_avail_id`,`khadamat_id`,`val`,`en`) VALUES ($hotel_avail_id,5,'$capacity[extra_bed]',3)");
                        if($capacity['lunch_person']!=0)        mysqli_query($db,"INSERT INTO `khadamat_room` (`hotel_avail_id`,`khadamat_id`,`val`,`en`) VALUES ($hotel_avail_id,6,'$capacity[extra_bed]',3)");
                        if($capacity['lunch_dinner_person']!=0) mysqli_query($db,"INSERT INTO `khadamat_room` (`hotel_avail_id`,`khadamat_id`,`val`,`en`) VALUES ($hotel_avail_id,7,'$capacity[extra_bed]',3)");
                        if($capacity['all']!=0)                 mysqli_query($db,"INSERT INTO `khadamat_room` (`hotel_avail_id`,`khadamat_id`,`val`,`en`) VALUES ($hotel_avail_id,9,'$capacity[extra_bed]',3)");
                        if($capacity['uall']!=0)                mysqli_query($db,"INSERT INTO `khadamat_room` (`hotel_avail_id`,`khadamat_id`,`val`,`en`) VALUES ($hotel_avail_id,10,'$capacity[extra_bed]',3)");
                        if($capacity['persian_hotel_label']!='')mysqli_query($db,"INSERT INTO `khadamat_room` (`hotel_avail_id`,`khadamat_id`,`val`,`en`) VALUES ($hotel_avail_id,8,'$capacity[extra_bed]',3)");                      
                        }
                    }
                }
            //__________ UPDATE en1
            mysqli_query($db,"UPDATE `hotel_avail` SET `en`=2 WHERE `en`=1 AND `source_id`=".$this->source_id." ");
            mysqli_query($db,"UPDATE `hotel_avail` SET `en`=1 WHERE `en`=3 AND `source_id`=".$this->source_id." ");
            mysqli_query($db,"DELETE FROM `hotel_avail`  WHERE `en`=2 AND `source_id`=".$this->source_id." ");
            mysqli_query($db,"DELETE FROM `khadamat_room`  WHERE `en`=1 AND `source_id`=".$this->source_id." ");
            mysqli_query($db,"UPDATE `khadamat_room` SET `en`=1 WHERE `en`=3 AND `source_id`=".$this->source_id." ");
        }
}

$asa=new asa_read_data_class($db);