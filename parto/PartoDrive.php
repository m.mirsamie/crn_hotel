<pre>
    <?php
    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of PartoDrive
     *
     * @author toranj
     */
    class PartoDrive {

        private $OfficeId = "CRS000030";
        private $UserName = "DARVISHI_XML";
        private $Password = "XML1234";
        private $session_id;
        public $source_id = 6;
        public $base_url = "http://partocrs.com/Api/Rest";

        //put your code here
        /*
         * Session Start in Parto
         */
        public function __construct() {
            $this->SessionCreate();
            //echo $this->session_id;
        }

        private function SessionCreate() {
            $req['OfficeId'] = $this->OfficeId;
            $req['UserName'] = $this->UserName;
            $req['Password'] = $this->Password;
            $request = json_encode($req);
            $response = $this->sendRequest('SessionCreate', $request);
            if ($response['Status'] === TRUE) {
                $this->session_id = $response['SessionId'];
            } else {
                // $this->log($this->source_id,$response['Error']['Id'],$response['Error']['Message'],date('Y-m-d H:i:s'));
                //print_r($response);
            }
        }

        private function EndSession() {
            $req['SessionId'] = $this->session_id;
            $close = $this->sendRequest($url, json_encode($req));
            if ($close['Result'] != true) {
                
            }
        }

        //Searching method for city pairs
        public function AirLowFareSearch($froms, $tos, $adult, $child, $inf, $cabinType, $airTripType) {
            $cabinType = array('Y' => 1, 'S' => 2, 'C' => 3, 'J' => 4, 'F' => 5, 'P' => 6);
            $MaxStopsQuantity = array('All' => 0, 'OneStop' => 1, 'Direct' => 2);
            $AirTripType = array('OneWay' => 1, 'Return' => 2, 'Circle' => 3, 'OpenJaw' => 4);

//           $req["NearByAirports"]= 1;
//            $req["PricingSourceType"] = 0;
//    $req["RequestOption"] = 0;
//    $req["SessionId"] =" sample string 2";
//    $req["AdultCount"] = 3;
//    $req["ChildCount"] = 4;
//    $req["InfantCount"] = 5;
//     
//    $req["TravelPreference"]["CabinType"] = 1;
//    $req["TravelPreference"]["MaxStopsQuantity"] = 0;
//    $req["TravelPreference"]["AirTripType"] = 1;  
//    
//    $req["TravelPreference"]["VendorExcludeCodes"][0] = "sample string 1";
//    $req["TravelPreference"]["VendorExcludeCodes"][1] = "sample string 2";
//    
//    $req["TravelPreference"]["VendorPreferenceCodes"][0] =" sample string 1";
//    $req["TravelPreference"]["VendorPreferenceCodes"][1] =" sample string 2";
//                
//    $req["OriginDestinationInformations"][0]["DepartureDateTime"] =" 2015-11-04T09:05:47.3116096+03:30";
//    $req["OriginDestinationInformations"][0]["DepartureDateTime"]["DestinationLocationCode"] = "sample string 2";
//    $req["OriginDestinationInformations"][0]["DepartureDateTime"]["OriginLocationCode"] =" sample string 3";



            $req["NearByAirports"] = 1;
            $req["PricingSourceType"] = 0;
            $req["RequestOption"] = 0;
            $req["SessionId"] = $this->session_id;
            $req["AdultCount"] = 3;
            $req["ChildCount"] = 4;
            $req["InfantCount"] = 5;

            $req["TravelPreference"]["CabinType"] = 1;
            $req["TravelPreference"]["MaxStopsQuantity"] = 0;
            $req["TravelPreference"]["AirTripType"] = 1;

            $req["TravelPreference"]["VendorExcludeCodes"][0] = "";
            $req["TravelPreference"]["VendorPreferenceCodes"][0] = "";

            $desCounter = 0;
            for ($i = 0; $i < 5; $i++) {
                $searchDate = date("Y-m-d", strtotime(date("Y-m-d") . " + $i days"));
                foreach ($froms as $from) {
                    foreach ($tos as $to) {
                       // echo "$from :: $to <br>";
                        if ($from == $to)
                            continue;
                        $req["OriginDestinationInformations"][$desCounter]["DepartureDateTime"] = $searchDate;
                        $req["OriginDestinationInformations"][$desCounter]["DestinationLocationCode"] = "$to";
                        $req["OriginDestinationInformations"][$desCounter]["OriginLocationCode"] ="$from";
                        $desCounter++;
                    }
                }
            }
            //print_r($req);
            $this->sendRequest('AirLowFareSearch',  json_encode($req));
            
            
        }

        //Checking the validity of the fares
        public function AirRevalidate($param) {
            $req["SessionId"] = $this->session_id;
            $req["FareSourceCode"] = "sample string 2";
        }

        //Ravlidating multiply fares
        public function MultiAirRevalidate($param) {
            $req["FareSourceCodes"][0] = "";
            $req["FareSourceCodes"][1] = "";
            $req["SessionId"] = $this->session_id;
        }

        //Retrieving Fare Rule from GDS
        public function AirRules($param) {
            $req["FareSourceCode"] = '';
            $req["SessionId"] = $this->session_id;
        }

        //Special method for pricing (Contact for permission)
        public function IntelliFare($param) {
            $BookingClassPreference = array('Default' => 0, 'Only' => 1, 'Any' => 2);

            $req["BookingClassPreference"] = 0;
            $req["SessionId"] = $this->session_id;
            $req["AdultCount"] = 2;
            $req["ChildCount"] = 3;
            $req["InfantCount"] = 4;

            $req["IntelliFareInformations"][0]["AirlineCode"] = "sample string";
            $req["IntelliFareInformations"][0]["ArrivalDateTime"] = "2015-11-04T09:33:09.9106153+03:30";
            $req["IntelliFareInformations"][0]["BookingClass"] = "sample string";
            $req["IntelliFareInformations"][0]["DepartureDateTime"] = "2015-11-04T09:33:09.9106153+03:30";
            $req["IntelliFareInformations"][0]["DestinationLocationCode"] = "sample string";
            $req["IntelliFareInformations"][0]["FlightNumber"] = "sample string";
            $req["IntelliFareInformations"][0]["OriginLocationCode"] = "sample string";

            $req["IntelliFareInformations"][1]["AirlineCode"] = "sample string";
            $req["IntelliFareInformations"][1]["ArrivalDateTime"] = "2015-11-04T09:33:09.9106153+03:30";
            $req["IntelliFareInformations"][1]["BookingClass"] = "sample string";
            $req["IntelliFareInformations"][1]["DepartureDateTime"] = "2015-11-04T09:33:09.9106153+03:30";
            $req["IntelliFareInformations"][1]["DestinationLocationCode"] = "sample string";
            $req["IntelliFareInformations"][1]["FlightNumber"] = "sample string";
            $req["IntelliFareInformations"][1]["OriginLocationCode"] = "sample string";
        }
        //Special method for pricing (Contact for permission)
        public function IntelliBestBuy($param) {
$BookingClassPreference=array('Default'=>0,'Only'=>1,'Any'=>2);
    $req["BookingClassPreference"] = 0;
    $req["CabinPreference"] = 1;
    $req["SessionId"] =1;
    $req["AdultCount"] =1;
    $req["ChildCount"] = 3;
    $req["InfantCount"] = 4;
    
    $req["IntelliBestBuyInformations"][0]["AirlineCode"] =" sample string 1";
    $req["IntelliBestBuyInformations"][0]["ArrivalDateTime"] =" 2015-11-04T10:04:08.2188405+03:30";
                    $req["IntelliBestBuyInformations"][0]["BookingClass"] =" sample string 3";
                    $req["IntelliBestBuyInformations"][0]["DepartureDateTime"] =" 2015-11-04T10:04:08.2205832+03:30";
                    $req["IntelliBestBuyInformations"][0]["DestinationLocationCode"] =" sample string 5";
                    $req["IntelliBestBuyInformations"][0]["FlightNumber"] =" sample string 6";
                    $req["IntelliBestBuyInformations"][0]["OriginLocationCode"] =" sample string 7";
            

            
        }
        //For making reservation (Issuing WebFare tickets)
        public function AirBook($param) {
            
            
        }

        public function avail($froms, $tos) {
            $cabinTypes = array('Y' => 1, 'S' => 2, 'C' => 3, 'J' => 4, 'F' => 5, 'P' => 6);
            $MaxStopsQuantity = array('All' => 0, 'OneStop' => 1, 'Direct' => 2);
            $AirTripTypes = array('OneWay' => 1, 'Return' => 2, 'Circle' => 3, 'OpenJaw' => 4);

            foreach ($froms as $from) {
                foreach ($tos as $to) {
                    echo "$from :: $to <br>";
                    if ($from == $to)
                        continue;
                    for ($i = 0; $i <= 2; $i++) {
                        $searchDate = date("Y-m-d", strtotime(date("Y-m-d") . " + $i days"));
                        echo $searchDate;
                        foreach ($cabinTypes as $cabinType => $cabin) {
                            echo $cabinType;
                            foreach ($AirTripTypes as $AirTripType => $AirTrip) {
                                echo $AirTripType;
                            }
                        }


                        $result = $this->search($searchDate, $from, $to);
                        //var_dump($result);
                        $info = $this->handleResult($result);
                        if ($info != FALSE) {
                            $flight_count = count($info['flights']);
                            for ($j = 0; $j < $flight_count; $j++) {
                                $this->db->addFlight($info['flights'][$j], $info['flight_extras'][$j], $info['specific_fields'][$j], $this->_source_id);
                            }
                        } else {
                            $result = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><OTA_AirLowFareSearchRS xmlns="http://www.opentravel.org/OTA/2003/05" EchoToken="50987" TimeStamp="2015-10-26T13:37:51.558Z" Target="Test" Version="2.001" SequenceNmbr="1" PrimaryLangID="En-us"><Success/><PricedItineraries><PricedItinerary SequenceNumber="1"><AirItinerary><OriginDestinationOptions><OriginDestinationOption><FlightSegment FlightNumber="JI1313" ResBookDesigCode="H" DepartureDateTime="2015-10-26T18:30:00.000+03:30" ArrivalDateTime="2015-10-26T19:45:00.000+03:30" StopQuantity="0" RPH="1"><DepartureAirport LocationCode="THR" Terminal="T1"/><ArrivalAirport LocationCode="SYZ" Terminal="T2"/><OperatingAirline Code="JI"/><Equipment AirEquipType="A320"/><BookingClassAvails><BookingClassAvail ResBookDesigCode="H" ResBookDesigQuantity="9"/></BookingClassAvails></FlightSegment></OriginDestinationOption></OriginDestinationOptions></AirItinerary><AirItineraryPricingInfo><ItinTotalFare><BaseFare CurrencyCode="IRR" DecimalPlaces="0" Amount="1508000"/><TotalFare CurrencyCode="IRR" DecimalPlaces="0" Amount="1669000"/></ItinTotalFare><PTC_FareBreakdowns><PTC_FareBreakdown><PassengerTypeQuantity Code="ADT" Quantity="1"/><FareBasisCodes><FareBasisCode FlightSegmentRPH="1">HOW</FareBasisCode></FareBasisCodes><PassengerFare><BaseFare CurrencyCode="IRR" DecimalPlaces="0" Amount="1508000"/><Taxes><Tax TaxCode="T8" TaxName="Other Tax" CurrencyCode="IRR" DecimalPlaces="0">91000</Tax><Tax TaxCode="YQ" TaxName="Airport Tax" CurrencyCode="IRR" DecimalPlaces="0">70000</Tax></Taxes><TotalFare CurrencyCode="IRR" DecimalPlaces="0" Amount="1669000"/></PassengerFare></PTC_FareBreakdown></PTC_FareBreakdowns></AirItineraryPricingInfo></PricedItinerary><PricedItinerary SequenceNumber="2"><AirItinerary><OriginDestinationOptions><OriginDestinationOption><FlightSegment FlightNumber="JI6666" ResBookDesigCode="H" DepartureDateTime="2015-10-26T18:30:00.000+03:30" ArrivalDateTime="2015-10-26T19:45:00.000+03:30" StopQuantity="0" RPH="2"><DepartureAirport LocationCode="THR" Terminal="T1"/><ArrivalAirport LocationCode="SYZ" Terminal="T2"/><OperatingAirline Code="JI"/><Equipment AirEquipType="A320"/><BookingClassAvails><BookingClassAvail ResBookDesigCode="H" ResBookDesigQuantity="9"/></BookingClassAvails></FlightSegment></OriginDestinationOption></OriginDestinationOptions></AirItinerary><AirItineraryPricingInfo><ItinTotalFare><BaseFare CurrencyCode="IRR" DecimalPlaces="0" Amount="1508000"/><TotalFare CurrencyCode="IRR" DecimalPlaces="0" Amount="1669000"/></ItinTotalFare><PTC_FareBreakdowns><PTC_FareBreakdown><PassengerTypeQuantity Code="ADT" Quantity="1"/><FareBasisCodes><FareBasisCode FlightSegmentRPH="2">HOW</FareBasisCode></FareBasisCodes><PassengerFare><BaseFare CurrencyCode="IRR" DecimalPlaces="0" Amount="1508000"/><Taxes><Tax TaxCode="T8" TaxName="Other Tax" CurrencyCode="IRR" DecimalPlaces="0">91000</Tax><Tax TaxCode="YQ" TaxName="Airport Tax" CurrencyCode="IRR" DecimalPlaces="0">70000</Tax></Taxes><TotalFare CurrencyCode="IRR" DecimalPlaces="0" Amount="1669000"/></PassengerFare></PTC_FareBreakdown></PTC_FareBreakdowns></AirItineraryPricingInfo></PricedItinerary></PricedItineraries></OTA_AirLowFareSearchRS>';
                            $info = $this->handleResult($result);
                            $flight_count = count($info['flights']);
                            for ($j = 0; $j < $flight_count; $j++) {
                                $this->db->addFlight($info['flights'][$j], $info['flight_extras'][$j], $info['specific_fields'][$j], $this->_source_id);
                            }
                        }
                    }
                }
            }
            $this->db->updateEN($this->_source_id);
        }

        public function sendRequest($url, $req) {
            $url = $this->base_url . '/' . $url;
            $ch = curl_init($url);

            curl_setopt_array($ch, array(
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $req,
                CURLOPT_HEADER => true,
                CURLOPT_HTTPHEADER => array('Content-Type: application/json; charset=utf-8', 'Cache-Control: no-cache')
                    )
            );

            $result = curl_exec($ch);
            echo '<br>'.$url.'<br>_________________<br>';
             print_r($result);
             echo "<hr>";
            curl_close($ch);
            return $result;
        }

    }

    $parto = new PartoDrive;

    $froms=array('MHD','THR','SYZ');
    $tos=array('MHD','THR','SYZ');
    $adult=1;
    $child=1;
    $inf=1;
    $cabinType=1;
    $airTripType=1;
    $parto->AirLowFareSearch($froms, $tos, $adult, $child, $inf, $cabinType, $airTripType);