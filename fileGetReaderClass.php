<?php

/*
 * @Anabestani
 * read data from Content other page
 */
class file_get_reader {
    function file_get_contents_utf8($url) { 
    $opts = array( 
        'http' => array( 
            'method'=>"GET", 
            'header'=>"Content-Type: text/html; charset=utf-8" 
        ) 
    ); 

    $context = stream_context_create($opts); 
    $result = @file_get_contents($url,false,$context); 
    return $result; 
    } 
    
}
