<?php
// Anabestani
// Bot for Read URL Address
final class SBot { 
      private $_data; 
      private $_errors = array(); 
      private $_info; 
      private $_is_success = false; 
      public $header_output = false; 
      public $proxy; 
      public $referer; 
      public $timeout = 20; 
      public $user_agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.8) Gecko/2009032609 Firefox/3.0.8'; 
      public function __construct($set_time_limit = 0, $ignore_user_abort = true) { 
            // init settings 
            set_time_limit((int)$set_time_limit); 
            ignore_user_abort((bool)$ignore_user_abort); 
      } 
      public function crawl($url, $post_vars = array()) { 
            if(!$url) { 
                  $this->_errors[] = 'Invalid crawl URL'; 
                  return; 
            } 

            if(!function_exists('curl_init')) { 
                  $this->_errors[] = 'Failed to load cURL library'; 
                  return; 
            } 

            if((int)$this->timeout < 1) { 
                  $this->_errors[] = 'Invalid timeout value'; 
                  return; 
            } 

            $ch = curl_init(); 

            // set cURL params 
            curl_setopt($ch, CURLOPT_URL, $url); 
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->timeout); 

            // set POST params 
            if(is_array($post_vars) && count($post_vars) > 0) { 
                  $post_params = null; 
                  foreach($post_vars as $k => $v) { 
                        $post_params .= $k . '=' . $v . '&'; 
                  } 
                  $post_params = rtrim($post_params, '&'); 

                  curl_setopt($ch, CURLOPT_POST, count($post_vars)); 
                  curl_setopt($ch, CURLOPT_POSTFIELDS, $post_params); 
            } 


            // output header 
            if((bool)$this->header_output) { 
                  curl_setopt($ch, CURLOPT_HEADER, 1); 
            } 

            // set proxy 
            if($this->proxy) { 
                  curl_setopt($ch, CURLOPT_PROXY, $this->proxy); 
                  curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1); 
            } 

            // set referer 
            if($this->referer) { 
                  curl_setopt($ch, CURLOPT_REFERER, $this->referer); 
            } 

            // set user agent 
            if($this->user_agent) { 
                  curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent); 
            } 

            // crawl 
            $this->_data = curl_exec($ch); 

            // check for failed crawl 
            if($this->_data === false || curl_errno($ch)) { 
                  $this->_data = null; 
                  $error = curl_error($ch); 
                  $this->_errors[] = $error ? $error : 'Crawl failed: unknown error'; 
            // successful crawl 
            } else { 
                  $this->_is_success = true; 
                  $this->_info = curl_getinfo($ch); 
            } 

            curl_close($ch); 
      } 
      public function getData() { 
            return $this->_data; 
      } 

      public function getErrorLast() { 
            return count($this->_errors > 0) ? end($this->_errors) : ''; 
      } 

      public function getErrors() { 
            return $this->_errors; 
      } 

      public function getInfo() { 
            return $this->_info; 
      } 

      public function isSuccess() { 
            return $this->_is_success; 
      } 
} 
?>