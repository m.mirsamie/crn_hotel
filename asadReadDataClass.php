<?php

/*
 * @Anabestani
 */
class asad_read_data_class extends file_get_reader {
 
    /*
     * Read Room Kind From http://94.183.153.237/safar/roomkinds_list.php
     * Type include is :: $row['id,$persian_name,$foreign_name']."`".$row['numberof_bed']
     * Return An Array of kinds
     */
    function room_kinds(){
        $url="http://94.183.153.237/safar/roomkinds_list.php";
        $result = $this->file_get_contents_utf8($url); 
        $result= json_decode($result);
//        return $result;
        if( $result == false){ return false; }
        else {
           // $result= explode('|',$result); 
            foreach ($result as $room){
                //list($id,$persian_name,$foreign_name,$numberof_bed)=  explode('`', $room);
                $id=$room['1'];
                $rooms[$id]['persian_name']=$room[1];
                $rooms[$id]['foreign_name']=$room[2];
                $rooms[$id]['numberof_bed']=$room[3];
                $rooms[$id]['time_stamp']=$room[4];
                //$rooms[$id]['id']=$id;
            }
            return $rooms;
        }
    }

        /*
     * Read Room Capacity From http://94.183.153.237/safar/capacity_list.php
     * Type include is :: $row['room_kind_id']."`".$row['id_hotel']."`".$row['date']."`".$row['iranian_date']."`".$row['iranian_board_rate']."`".$row['capacity']."`".
	$row['extra_bed']."`".$row['percent']."`".$row['lunch_room']."`".$row['dinner_room']."`".$row['dinner_person']."`".$row['lunch_person']."`".
	$row['lunch_dinner_person']."`".$row['all']."`".$row['uall']."`".$row['persian_hotel_label'];
         * 
     * Return An Array of Capacity List
     */
    function capacity_list(){
        $url="http://94.183.153.237/safar/capacity_list.php";
        $result = $this->file_get_contents_utf8($url); 
        if( $result == false){ return false; }
        else {
            $result= json_decode($result);
            //$result= explode('|',$result); 
            $i=0;
            foreach ($result as $capacity){
              //  list($room_kind_id,$id_hotel,$date,$iranian_date,$iranian_board_rate,$capacity,$extra_bed,$percent,$lunch_room,$dinner_room,$dinner_person,$lunch_person,$lunch_dinner_person,$all,$uall,$persian_hotel_label)=  explode('`', $capacity);
                $i++;
                $room_kind_id=$i;
                $rooms[$room_kind_id]['room_kind_id']=$capacity[0];
                $rooms[$room_kind_id]['id_hotel']=$capacity[1];
                $rooms[$room_kind_id]['date']=$capacity[2];
                $rooms[$room_kind_id]['iranian_date']=$capacity[3];
                $rooms[$room_kind_id]['iranian_board_rate']=$capacity[4];
                $rooms[$room_kind_id]['capacity']=$capacity[5];
                $rooms[$room_kind_id]['extra_bed']=$capacity[6];
                $rooms[$room_kind_id]['percent']=$capacity[7];
                $rooms[$room_kind_id]['lunch_room']=$capacity[8];
                $rooms[$room_kind_id]['dinner_room']=$capacity[9];
                $rooms[$room_kind_id]['dinner_person']=$capacity[10];
                $rooms[$room_kind_id]['lunch_person']=$capacity[11];
                $rooms[$room_kind_id]['lunch_dinner_person']=$capacity[12];
                $rooms[$room_kind_id]['all']=$capacity[13];
                $rooms[$room_kind_id]['uall']=$capacity[14];
                $rooms[$room_kind_id]['persian_hotel_label']=$capacity[15];
                $rooms[$room_kind_id]['time_stamp']=$capacity[16];
                
//                $rooms[$room_kind_id]['room_kind_id']=$room_kind_id;
//                $rooms[$room_kind_id]['id_hotel']=$id_hotel;
//                $rooms[$room_kind_id]['date']=$date;
//                $rooms[$room_kind_id]['iranian_date']=$iranian_date;
//                $rooms[$room_kind_id]['iranian_board_rate']=$iranian_board_rate;
//                $rooms[$room_kind_id]['capacity']=$capacity;
//                $rooms[$room_kind_id]['extra_bed']=$extra_bed;
//                $rooms[$room_kind_id]['percent']=$percent;
//                $rooms[$room_kind_id]['lunch_room']=$lunch_room;
//                $rooms[$room_kind_id]['dinner_room']=$dinner_room;
//                $rooms[$room_kind_id]['dinner_person']=$dinner_person;
//                $rooms[$room_kind_id]['lunch_person']=$lunch_person;
//                $rooms[$room_kind_id]['lunch_dinner_person']=$lunch_dinner_person;
//                $rooms[$room_kind_id]['all']=$all;
//                $rooms[$room_kind_id]['uall']=$uall;
//                $rooms[$room_kind_id]['persian_hotel_label']=$persian_hotel_label;
            }
            return $rooms;
        }
    }
    
    
            /*
     * Read Hotels List From http://94.183.153.237/safar/hotels_list.php
     * Type include is :: id-id_area-persian_name-foreign_name-hotel_kind-short_desc-full_desc-star-adress-room_number-status-order_priority
     * Return An Array of Hotel List
     */
    function hotels_list(){
        $url="http://94.183.153.237/safar/hotels_list.php";
        $result = $this->file_get_contents_utf8($url); 
        if( $result == false){ return false; }
        else {
            $result= json_decode($result);
            //$result= explode('|',$result); 
            foreach ($result as $hotel){
              //  list($id,$id_area,$persian_name,$foreign_name,$hotel_kind,$short_desc,$full_desc,$star,$adress,$room_number,$status,$order_priority)=explode('`', $hotel);
                $id=$hotel[0];
                $hotels[$id]['id_area']=$hotel[1];
                $hotels[$id]['persian_name']=$hotel[2];
                $hotels[$id]['foreign_name']=$hotel[3];
                $hotels[$id]['hotel_kind']=$hotel[4];
                $hotels[$id]['short_desc']=$hotel[5];
                $hotels[$id]['full_desc']=$hotel[6];
                $hotels[$id]['star']=$hotel[7];
                $hotels[$id]['adress']=$hotel[8];
                $hotels[$id]['room_number']=$hotel[9];
                $hotels[$id]['status']=$hotel[10];
                $hotels[$id]['order_priority']=$hotel[11];
                $hotels[$id]['time_stamp']=$hotel[12];

//                $hotels[$id]['id_area']=$id_area;
//                $hotels[$id]['persian_name']=$persian_name;
//                $hotels[$id]['foreign_name']=$foreign_name;
//                $hotels[$id]['hotel_kind']=$hotel_kind;
//                $hotels[$id]['short_desc']=$short_desc;
//                $hotels[$id]['full_desc']=$full_desc;
//                $hotels[$id]['star']=$star;
//                $hotels[$id]['adress']=$adress;
//                $hotels[$id]['room_number']=$room_number;
//                $hotels[$id]['status']=$status;
//                $hotels[$id]['order_priority']=$order_priority;
            }
            return $hotels;
        }
    }
    
}
