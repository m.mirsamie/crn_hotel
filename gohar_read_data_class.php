<pre>
<?php
require './db_hotel_cron.php.php';
$db= new db_hotel_cron ('localhost','root','3068145','safar');
class gohar_read_data_class {
    //put your code here
    public $db_gohar;
    public $source_id=1;
    public $db;
    public function __construct($db) {
        $this->db=$db;
        $this->db_gohar = mysqli_connect('localhost','root','3068145','gohar_hotel');
        mysqli_query($this->db_gohar,"SET NAMES utf8");
        //$this->read_hotels();
        $this->room_type();
        //$this->room_avail();
    }
    
    function read_hotels(){
        global $db;
        $gohar_hotel=array();
        $sql=mysqli_query($this->db_gohar,"SELECT `id`,`fa_hotel`,`en_hotel` FROM `htl_hotel` ");
        while ($row=  mysqli_fetch_assoc($sql)){
            $gohar_hotel[$row['id']]['name']=$row['fa_hotel'];
            $gohar_hotel[$row['id']]['en_name']=$row['en_hotel'];
        }
        $this->db->addHotelSource($this->source_id,$gohar_hotel);
    }
    
    function read_hotels2(){
        global $db;
        $gohar_hotel=array();
        $safar_hotel=array();
        $new_hotel=array();
        
        $sql=mysqli_query($this->db_gohar,"SELECT `id`,`fa_hotel`,`en_hotel`,`stars` FROM `htl_hotel` ");
        while ($row=mysqli_fetch_assoc($sql)){
            $gohar_hotel[$row['id']]['name']=$row['fa_hotel'];
            $gohar_hotel[$row['id']]['en_name']=$row['en_hotel'];
            $gohar_hotel[$row['id']]['stars']=$row['stars'];
        }
        echo 'HOTELS COUNT :: '.count($gohar_hotel).'<br>';
        
        foreach ($gohar_hotel as $id=>$hotel){
            //$insert-> execute(array(':name'=>$hotel['name'],':en_name'=>$hotel['en_name'],':star'=>$hotel['stars']));
            $sql= "INSERT INTO `hotels` (`id`, `name`, `en_name`, `star`) VALUES (NULL,'$hotel[name]','$hotel[en_name]','$hotel[stars]');)";
            echo $sql.'<br>';
            mysqli_query($db,$sql);
            $hotels_id=  mysqli_insert_id($db);
            $sql="INSERT INTO hotels_source (`hotels_id`,`source_id`,`name`,`en_name`,`id_source_hotel`) VALUES($hotels_id,1,'$hotel[name]','$hotel[en_name]',$id)";
            echo '<br>'.$sql.'<br>';
            mysqli_query($db,$sql);
            echo $hotels_id.'<br>';
           // echo $insert2->execute(array(':hotels_id'=>$hotels_id,':name'=>$hotel['name'],':en_name'=>$hotel['en_name'],':id_source_hotel'=>$id));
        }
    }
    function room_type(){
        $gohar_room_type=array();
        $sql_gohar=  mysqli_query($this->db_gohar,"SELECT * FROM `htl_room_type` ");
        while($row=mysqli_fetch_assoc($sql_gohar)){
            $gohar_room_type[$row['id']]['hotel_id']=$row['hotel_id'];
            $gohar_room_type[$row['id']]['type']=$row['type'];
            $gohar_room_type[$row['id']]['name']=$row['name'];
            $gohar_room_type[$row['id']]['property'][1]=$row['type'];
            
            if($row['type_price']!=0) $gohar_room_type[$row['id']]['property'][2]=$row['type_price'];
            if($row['type_force']!=0) $gohar_room_type[$row['id']]['property'][3]=$row['type_force'];
            if($row['type_off']!=0) $gohar_room_type[$row['id']]['property'][4]=$row['type_off'];
            
            if($row['transfer']!=0) $gohar_room_type[$row['id']]['property'][6]=$row['transfer'];
            if($row['transfer_price']!=0) $gohar_room_type[$row['id']]['property'][7]=$row['transfer_price'];
            if($row['transfer_force']!=0) $gohar_room_type[$row['id']]['property'][8]=$row['transfer_force'];
            if($row['transfer_off']!=0) $gohar_room_type[$row['id']]['property'][9]=$row['transfer_off'];
            
            if($row['adult']!=0) $gohar_room_type[$row['id']]['property'][16]=$row['adult'];
            if($row['child']!=0) $gohar_room_type[$row['id']]['property'][17]=$row['child'];
            if($row['child_bed']!=0) $gohar_room_type[$row['id']]['property'][18]=$row['child_bed'];
            if($row['extra_bed']!=0) $gohar_room_type[$row['id']]['property'][19]=$row['extra_bed'];
            if($row['max_extra']!=0) $gohar_room_type[$row['id']]['property'][20]=$row['max_extra'];
            if($row['price_child']!=0) $gohar_room_type[$row['id']]['property'][21]=$row['price_child'];
            if($row['price_child_bed']!=0) $gohar_room_type[$row['id']]['property'][22]=$row['price_child_bed'];
            if($row['price_extra']!=0) $gohar_room_type[$row['id']]['property'][23]=$row['price_extra'];
            $gohar_room_type[$row['id']]['property'][24]=$row['sell_type'];
            
            if($row['gasht']!=0) $gohar_room_type[$row['id']]['property'][11]=$row['gasht'];
            if($row['gasht_price']!=0) $gohar_room_type[$row['id']]['property'][12]=$row['gasht_price'];
            if($row['gasht_force']!=0) $gohar_room_type[$row['id']]['property'][13]=$row['gasht_force'];
            if($row['gasht_off']!=0) $gohar_room_type[$row['id']]['property'][14]=$row['gasht_off'];            
        }
        
        $this->db->addRoomType($this->source_id,$gohar_room_type,true);
        
//        $sql_safar=mysqli_query($db,"SELECT id,id_room_source FROM room_type where source_id=".$this->source_id." ");
//        while($row=mysqli_fetch_assoc($sql_safar)){
//            $safar_room_type[$row['id_room_source']]=$row['id'];
//        }
//        array_diff_key($gohar_room_type, $safar_room_type);
//        $new_room_type=  array_diff_key($gohar_room_type, $safar_room_type);
//        if(count($new_room_type)>0){
//
//            $hid=mysqli_query($db,"SELECT hotels_id,id_source_hotel FROM `hotels_source` where source_id=".$this->source_id." ");
//            while($row=mysqli_fetch_assoc($hid)){
//                $hotel_id[$row['id_source_hotel']]=$row['hotels_id'];
//            }
//            $insert2=$db->prepare("INSERT INTO room_type_hotel (room_type_id,hotels_id) VALUES (:room_type_id,:hotels_id) ");
//            foreach($new_room_type as $id=>$room_type){
//                if(isset($hotel_id[$room_type['hotel_id']]) && $hotel_id[$room_type['hotel_id']]!=0 ){
//                    mysqli_query($db,"INSERT INTO room_type (name,en_name,source_id,id_room_source) VALUES ('$room_type[name]','',".$this->source_id.",$id) ");
//                    $room_type_id=  mysqli_insert_id($db);
//                    mysqli_query($db,"INSERT INTO room_type_hotel (room_type_id,hotels_id) VALUES ($room_type_id,".$hotel_id[$room_type['hotel_id']].") ");
//                    }
//                else {echo 'هتل ثبت نشده';}
//                }
//            } 
        }
        
    
    function room_avail(){
            $hotel_id=  $this->db->listHotelMin($this->source_id);
            $room_id=  $this->db->listRoomMin($this->source_id);
            
            $avail=  mysqli_query($this->db_gohar,"SELECT * FROM `htl_new_room`");
            while($capacity=mysqli_fetch_assoc($avail)){
                $tarikh=@date('Y-m-d H:i:s',$row['date']);
                if(isset($room_id[$capacity['room_type_id']]) && isset($hotel_id[$capacity['hotel_id']])){
                    $this->db->addHotelAvail($room_id[$capacity['room_type_id']],$hotel_id[$capacity['hotel_id']],$tarikh,$capacity['capa'],$capacity['price'],1,3,$this->source_id);
                    $hotel_avail_id=  $this->db->insert_id;
                    
                    

                }
            }
            //__________ UPDATE en1
            mysqli_query($db,"UPDATE `hotel_avail` SET `en`=2 WHERE `en`=1 AND `source_id`=".$this->source_id." ");
            mysqli_query($db,"UPDATE `hotel_avail` SET `en`=1 WHERE `en`=3 AND `source_id`=".$this->source_id." ");
            mysqli_query($db,"DELETE FROM `hotel_avail`  WHERE `en`=2 AND `source_id`=".$this->source_id." ");           
        }
        
        
        
        
    
}

$gohar=new gohar_read_data_class($db);
//$gohar->read_hotels();
?>
